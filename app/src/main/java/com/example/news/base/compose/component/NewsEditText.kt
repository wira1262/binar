package com.example.news.base.compose.component

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.text.BasicTextField
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.unit.sp


@Composable
fun NewsEditText(
    modifier: Modifier,
    onValueChange: (String) -> Unit,
    placeholder: String,
    enabled: Boolean = true,
    onClick: () -> Unit = {}
) {
    val (notifyDebounceSearch, textEditVal) = editTextStateDebounce {
        onValueChange(it)
    }

    Box {
        BasicTextField(
            value = textEditVal,
            onValueChange = {
                notifyDebounceSearch(it)
            },
            textStyle = TextStyle(
                fontSize = 16.sp,
                fontWeight = FontWeight.W600,
                color = if (textEditVal.isEmpty()) Color.Gray else Color.Black
            ),

            singleLine = true,
            keyboardOptions = KeyboardOptions.Default.copy(imeAction = ImeAction.Done),
            enabled = enabled,
            decorationBox = { innerTextField ->
                if (textEditVal.isEmpty()) {
                    Text(placeholder, color = Color.Gray)
                }
                innerTextField()
            },
            modifier = modifier
                .fillMaxWidth()
                .clickable(onClick = onClick)
        )
    }

}