package com.example.news.base.compose.component

import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.runtime.setValue
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.debounce
import kotlinx.coroutines.flow.distinctUntilChanged
import kotlinx.coroutines.launch
import kotlin.reflect.KFunction1

data class TextDebouncerProp(
    val mutableFlow: KFunction1<String, Unit>,
    val value: String
)
@Composable
fun editTextStateDebounce(
    onTextChange: (String)->Unit
): TextDebouncerProp {
    val coroutineScope = rememberCoroutineScope()

    var textEditVal by remember {
        mutableStateOf("")
    }

    val debounceState = MutableStateFlow<String?>(null).apply {
        coroutineScope.launch{
            debounce(700)
                .distinctUntilChanged()
                .collect {
                    if(it == null) return@collect
                    onTextChange(it)
                }
        }
    }

    fun notify(content: String){
        coroutineScope.launch {
            debounceState.emit(content)
        }
        textEditVal = content
    }

    return TextDebouncerProp(::notify, textEditVal)
}