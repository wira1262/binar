@file:OptIn(ExperimentalMaterialApi::class)

package com.example.news.base.compose.component

import androidx.compose.foundation.lazy.LazyListState
import androidx.compose.foundation.lazy.rememberLazyListState
import androidx.compose.material.ExperimentalMaterialApi
import androidx.compose.material.pullrefresh.PullRefreshState
import androidx.compose.material.pullrefresh.rememberPullRefreshState
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.derivedStateOf
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateListOf
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.runtime.setValue
import androidx.compose.runtime.snapshots.SnapshotStateList
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlin.reflect.KFunction1

data class LazyListControlData<Data>(
    val appendData: KFunction1<List<Data>, Unit>,
    val releasePagingBlock: () -> Unit,
    val scrollState: LazyListState,
    val lazyList: SnapshotStateList<Data>,
    val reset: () -> Unit,
    val refreshState: PullRefreshState,
    val isLoading: Boolean,
    val kFunction0: KFunction1<Boolean, Unit>,
)

@OptIn(ExperimentalMaterialApi::class)
@Composable
fun <Data>lazyColumnControl(
    data : List<Data>,
    reachBottom : (Int)->Unit,
    onRefresh : (Int)->Unit,
) : LazyListControlData<Data> {


    val coroutineScope = rememberCoroutineScope()

    var currentPage by remember { mutableStateOf(1) }
    var isAvailableForFetch by remember { mutableStateOf(true) }

    var lazyList = remember {
        mutableStateListOf<Data>().apply {
            data.forEach {
                coroutineScope.launch {
                    delay(50)
                    this@apply.add(it)
                }
            }
        }
    }

    var isLoading by remember {
        mutableStateOf(false)
    }
    fun reset(){
        currentPage = 1;
        lazyList.clear();
        isAvailableForFetch = true;
    }
    val refreshState = rememberPullRefreshState(refreshing = isLoading, onRefresh = {
        reset()
        onRefresh(currentPage)
    })
    val scrollState = rememberLazyListState()
    fun LazyListState.isScrolledToEnd() = layoutInfo.visibleItemsInfo.lastOrNull()?.index == layoutInfo.totalItemsCount - 1
    val endOfListReached by remember {
        derivedStateOf {
            scrollState.isScrolledToEnd()
        }
    }

    LaunchedEffect(key1 = endOfListReached){
        if(!lazyList.isEmpty() && isAvailableForFetch && endOfListReached){
            isAvailableForFetch = false; isLoading = true;
            currentPage++;
            reachBottom(currentPage);
        }
    }

    fun setLoading(isLoad: Boolean){
        isLoading = isLoad
    }
    fun appendData(data : List<Data>){
        coroutineScope.launch {
            data.forEach {
                delay(250)
                lazyList.add(it)
            }
        }
    }
    fun releasePagingBlock(){
        isAvailableForFetch = true;
    }

    return LazyListControlData(
        ::appendData, ::releasePagingBlock, scrollState, lazyList,
        ::reset, refreshState, isLoading, ::setLoading
    )
}