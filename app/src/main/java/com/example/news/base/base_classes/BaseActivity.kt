package com.example.news.base.base_classes

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.compose.material.ExperimentalMaterialApi
import com.example.news.MyApp
import com.example.news.di.activity.ActivityComponent
import com.example.news.di.activity.ActivityModule
import com.example.news.di.activity.UseCaseModule
import com.example.news.di.activity.viewmodels.ViewModelBinder
import com.example.news.di.activity.viewmodels.ViewModelModule
import com.telehealtheasyaccess.mobileapp.di.App.AppComponent

@ExperimentalMaterialApi
open class BaseActivity : ComponentActivity() {

    val appComponent : AppComponent by lazy { (application as MyApp).myAppComponent}
    val activityComponent : ActivityComponent by lazy {
        appComponent.newActivityComponent(
            UseCaseModule(), ViewModelBinder(), ViewModelModule(), ActivityModule(this)
        )
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        inject(activityComponent)
    }
    open fun inject(activityComponent: ActivityComponent){}

}