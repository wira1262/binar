package com.example.news

import android.annotation.SuppressLint
import android.app.Application
import android.content.Context
import androidx.compose.material.ExperimentalMaterialApi
import com.telehealtheasyaccess.mobileapp.di.App.AppComponent
import com.telehealtheasyaccess.mobileapp.di.App.AppModule
import com.telehealtheasyaccess.mobileapp.di.App.DaggerAppComponent
import com.telehealtheasyaccess.mobileapp.di.App.NetworkBinder

@ExperimentalMaterialApi
class MyApp : Application() {

    val myAppComponent: AppComponent by lazy {
        DaggerAppComponent.builder().appModule(AppModule(this))
            .networkBinder(NetworkBinder()).build()
    }

    companion object {
        @SuppressLint("StaticFieldLeak")
        private lateinit var context: Context
        val appContext: Context
            get() = context
    }

    override fun onCreate() {
        super.onCreate()
        context = this.applicationContext
    }
}