package com.example.news.di.activity.viewmodels

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.news.domain.usecase.GetNewsUseCase
import com.example.news.domain.usecase.GetSourceUseCase
import com.example.news.feature.news.NewViewModel
import dagger.Module
import dagger.Provides
import dagger.multibindings.IntoMap


@Module
class ViewModelBinder {

    @Provides
    fun viewModelFactory(provider: ViewModelProviderFactory): ViewModelProvider.Factory {
        return provider
    }

    @Provides
    @IntoMap
    @ViewModelKey(NewViewModel::class)
    fun strokeDiseaseVm(
        getNewsUseCase: GetNewsUseCase,
        sourceUseCase: GetSourceUseCase
    ): ViewModel {
        return NewViewModel(
            getNewsUseCase, sourceUseCase
        )
    }
}