package com.example.news.di.activity

import androidx.activity.ComponentActivity
import dagger.Module
import dagger.Provides

@Module
class ActivityModule(
    val activity: ComponentActivity
){

    @Provides
    @ActivityScope
    fun activity(): ComponentActivity = activity

}