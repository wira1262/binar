package com.example.news.di.activity.viewmodels

import androidx.activity.ComponentActivity
import androidx.lifecycle.ViewModelProvider
import com.example.news.di.activity.ActivityScope
import com.example.news.feature.news.NewViewModel
import dagger.Module
import dagger.Provides

@Module
class ViewModelModule {

    @Provides
    @ActivityScope
    fun NewViewModel(
        activity: ComponentActivity,
        factory: ViewModelProvider.Factory
    ): NewViewModel {
        return ViewModelProvider(
            activity, factory
        )[NewViewModel::class.java]
    }
}