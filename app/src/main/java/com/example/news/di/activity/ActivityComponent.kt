package com.example.news.di.activity

import androidx.compose.material.ExperimentalMaterialApi
import com.example.news.di.activity.viewmodels.ViewModelBinder
import com.example.news.di.activity.viewmodels.ViewModelModule
import com.example.news.feature.news.MainActivity
import dagger.Subcomponent

@ExperimentalMaterialApi
@ActivityScope
@Subcomponent(
    modules = [
        UseCaseModule::class, ViewModelBinder::class, ViewModelModule::class,
        ActivityModule::class
    ]
)
interface ActivityComponent {
    fun inject(mainActivity: MainActivity)
}
