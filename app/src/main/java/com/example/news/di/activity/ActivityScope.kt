package com.example.news.di.activity

import javax.inject.Scope

@Scope
annotation class ActivityScope()

