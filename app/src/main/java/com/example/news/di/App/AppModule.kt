package com.telehealtheasyaccess.mobileapp.di.App

import android.app.Application
import dagger.Module
import dagger.Provides

@Module
class AppModule(val application: Application) {

    @Provides
    @AppScope
    fun application(): Application {
        return application
    }
}