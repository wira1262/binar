package com.example.news.di.activity

import com.example.news.data.remote.api.NewsApi
import com.example.news.domain.usecase.GetNewsUseCase
import com.example.news.domain.usecase.GetSourceUseCase
import dagger.Module
import dagger.Provides

@Module
class UseCaseModule {

    @Provides
    @ActivityScope
    fun getNewsUseCase(
        api : NewsApi,
    ) : GetNewsUseCase =
        GetNewsUseCase(api)

    @Provides
    @ActivityScope
    fun getSourceUseCase(
        api : NewsApi,
    ) : GetSourceUseCase =
        GetSourceUseCase(api)

}