package com.telehealtheasyaccess.mobileapp.di.App

import javax.inject.Scope

@Scope
annotation class AppScope()