package com.telehealtheasyaccess.mobileapp.di.App

import com.example.news.data.remote.api.NewsApi
import com.example.news.data.remote.api.NewsApiImpl
import com.example.news.data.remote.client.RetrofitClient
import com.example.news.data.remote.client.retrofitinterfaces.NewApiRetrofit
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import java.util.concurrent.TimeUnit

@Module
class NetworkBinder {

    @Provides
    @AppScope
    fun getClientLogger(): HttpLoggingInterceptor {
        return HttpLoggingInterceptor()
            .setLevel(HttpLoggingInterceptor.Level.BODY)
    }

    @Provides
    @AppScope
    fun getRetrofitClient(): RetrofitClient {
        return RetrofitClient()
    }

    @Provides
    @AppScope
    fun getOkHttpClientBuilder(
        logger: HttpLoggingInterceptor,
    ): OkHttpClient.Builder {
        val client = OkHttpClient.Builder()
        client
            .addInterceptor(logger)
            .readTimeout(TIMEOUT_HTTP_CALL, TimeUnit.SECONDS)
            .writeTimeout(TIMEOUT_HTTP_CALL, TimeUnit.SECONDS)
            .connectTimeout(TIMEOUT_HTTP_CALL, TimeUnit.SECONDS); // Maximum connect timeout
        return client
    }

    @Provides
    @AppScope
    fun NewApiRetrofit(
        retrofitClient: RetrofitClient,
        okHttpClient: OkHttpClient.Builder
    ): NewApiRetrofit {
        return retrofitClient.getNewApiRetrofit(
            okHttpClient
        )
    }

    @Provides
    @AppScope
    fun getNewsApi(
        newsApiRetrofit: NewApiRetrofit
    ): NewsApi {
        return NewsApiImpl(
            newsApiRetrofit
        )
    }

    companion object {
        private const val TIMEOUT_HTTP_CALL = 60L
    }
}