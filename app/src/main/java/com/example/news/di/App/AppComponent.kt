package com.telehealtheasyaccess.mobileapp.di.App

import androidx.compose.material.ExperimentalMaterialApi
import com.example.news.di.activity.ActivityComponent
import com.example.news.di.activity.ActivityModule
import com.example.news.di.activity.UseCaseModule
import com.example.news.di.activity.viewmodels.ViewModelBinder
import com.example.news.di.activity.viewmodels.ViewModelModule
import dagger.Component

@ExperimentalMaterialApi
@AppScope
@Component(modules = [
    NetworkBinder::class, AppModule::class
])
interface AppComponent {
    fun newActivityComponent (
        useCaseModule: UseCaseModule,
        viewModelBinder: ViewModelBinder,
        viewModelModule: ViewModelModule,
        activityModule: ActivityModule
    ) : ActivityComponent
}