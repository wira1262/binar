package com.example.news.data.remote.client.retrofitinterfaces

import com.example.news.data.remote.model.response.NewsResponse
import com.example.news.data.remote.model.response.SourceResponseDto
import retrofit2.http.GET
import retrofit2.http.Query

interface NewApiRetrofit {
    @GET("v2/top-headlines")
    suspend fun getHeadlinesNews(
//        @Query("country") country : String = "us",
        @Query("page") page: Int,
        @Query("sources") sources: String,
        @Query("q") keyword: String,
        @Query("apiKey") apiKey : String = "096fe12cd02f4e90bb158f6c143d7c21",
        @Query("pageSize") pageSize : Int = 8,
    ): NewsResponse

    @GET("v2/top-headlines/sources")
    suspend fun getSourcesList(
        @Query("category") category: String,
        @Query("apiKey") apiKey : String = "096fe12cd02f4e90bb158f6c143d7c21",
    ): SourceResponseDto

}