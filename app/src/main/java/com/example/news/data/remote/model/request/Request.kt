package com.example.news.data.remote.model.request

data class GetHeadlinesRequest (
    var page: Int = 1,
    var sources: String = "",
    var category: String = "",
    var keyword: String = "",
)