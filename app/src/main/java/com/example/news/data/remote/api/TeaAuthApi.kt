package com.example.news.data.remote.api

import com.example.news.data.remote.client.retrofitinterfaces.NewApiRetrofit
import com.example.news.data.remote.model.request.GetHeadlinesRequest
import com.example.news.data.remote.model.response.NewsResponse
import com.example.news.data.remote.model.response.SourceResponseDto

interface NewsApi {
    suspend fun getHeadlines(
        req: GetHeadlinesRequest
    ) : NewsResponse

    suspend fun getSources(
        category: String,
    ) : SourceResponseDto
}

class NewsApiImpl(
    val newApiRetrofit: NewApiRetrofit
) : NewsApi {

    override suspend fun getSources(category: String): SourceResponseDto {
        return newApiRetrofit.getSourcesList(category)
    }

    override suspend fun getHeadlines(
        req: GetHeadlinesRequest
    ): NewsResponse {
        req.run {
            return newApiRetrofit.getHeadlinesNews(page, sources, keyword)
        }
    }

}