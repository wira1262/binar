@file:OptIn(ExperimentalComposeUiApi::class)

package com.example.news.feature.news.component

import androidx.compose.foundation.layout.*
import androidx.compose.runtime.*
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.tooling.preview.Preview

@OptIn(ExperimentalComposeUiApi::class)
@Composable
fun TaskForm(
) {


}

@Preview()
@Composable
fun previewTaskForm(){
    TaskForm()
}
