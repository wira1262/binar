package com.example.news.feature.news

import android.os.Bundle
import androidx.activity.compose.setContent
import androidx.compose.material.ExperimentalMaterialApi
import androidx.navigation.NavHostController
import androidx.navigation.compose.rememberNavController
import com.example.news.base.base_classes.BaseActivity
import com.example.news.base.ui.theme.TasklistTheme
import com.example.news.di.activity.ActivityComponent
import com.example.news.feature.news.navigator.NewsComposeNavigationHost
import javax.inject.Inject

@ExperimentalMaterialApi
class MainActivity : BaseActivity() {

    @Inject
    lateinit var newsVm: NewViewModel

    private lateinit var navController : NavHostController

    override fun inject(activityComponent: ActivityComponent) {
        super.inject(activityComponent)
        activityComponent.inject(this)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            TasklistTheme {
                navController = rememberNavController()
                NewsComposeNavigationHost(newsVm, navController)
            }
        }
    }
}
