package com.example.news.feature.news.screens

import android.annotation.SuppressLint
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavGraphBuilder
import androidx.navigation.compose.composable
import com.example.news.base.compose.component.ShowFadeInSlideBottom
import com.example.news.feature.news.NewViewModel
import com.example.news.feature.news.navigator.CATEGORY_LIST_SCREEN

@SuppressLint("UnusedMaterialScaffoldPaddingParameter")
fun NavGraphBuilder.CategoryListScreen(
    newsVm : NewViewModel,
    onSelectedCategory : ()->Unit
){
    composable(route = CATEGORY_LIST_SCREEN){
        Column(
            Modifier.verticalScroll(rememberScrollState())
        ){
            /**
             * business
             * entertainment
             * general
             * health
             * science
             * sports
             * all
             */
            ShowFadeInSlideBottom(150) {
                ItemCategory("all", newsVm, onSelectedCategory)
            }
            ShowFadeInSlideBottom(150) {
                ItemCategory("business", newsVm, onSelectedCategory)
            }
            ShowFadeInSlideBottom(150) {
                ItemCategory("entertainment", newsVm, onSelectedCategory)
            }
            ShowFadeInSlideBottom(150) {
                ItemCategory("general", newsVm, onSelectedCategory)
            }
            ShowFadeInSlideBottom(150) {
                ItemCategory("health", newsVm, onSelectedCategory)
            }
            ShowFadeInSlideBottom(150) {
                ItemCategory("science", newsVm, onSelectedCategory)
            }
            ShowFadeInSlideBottom(150) {
                ItemCategory("sports", newsVm, onSelectedCategory)
            }
        }
    }
}
@Composable
fun ItemCategory(
    item: String,
    newsVm: NewViewModel,
    onSelectedCategory: () -> Unit
){
    Card(
        Modifier
            .padding(16.dp)
            .clickable {
                newsVm.reqHeadline.category = if (item == "all") "" else item
                onSelectedCategory()
            }
    ){
        Text(
            text = item,
            textAlign = TextAlign.Left,
            fontWeight = FontWeight.Bold,
            fontSize = 20.sp,
            modifier = Modifier
                .fillMaxWidth()
                .padding(vertical = 8.dp, horizontal = 8.dp)
        )
    }
}