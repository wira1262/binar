@file:OptIn(ExperimentalMaterialApi::class)

package com.example.news.feature.news.screens

import android.annotation.SuppressLint
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Info
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavGraphBuilder
import androidx.navigation.compose.composable
import base.compose.colorPrimary
import com.example.news.feature.news.NewViewModel
import com.example.news.feature.news.component.NewsList
import com.example.news.feature.news.navigator.NEWS_LIST_SCREEN

@SuppressLint("UnusedMaterialScaffoldPaddingParameter")
fun NavGraphBuilder.NewsListScreen(
    newsViewModel: NewViewModel,
    onOpenDetail: () -> Unit,
    onBack: () -> Unit,
) {
    composable(route = NEWS_LIST_SCREEN) {

        Column {
            NewsList(
                listOf(), newsViewModel,
                {
                    onOpenDetail()
                    newsViewModel.currentSelectedNew = it
                }
            ) {
                onBack()
            }
        }
    }
}

@Composable
fun EmptyIllustration(
    onRetry: () -> Unit
) {
    Column(
        horizontalAlignment = Alignment.CenterHorizontally,
        modifier = Modifier
            .fillMaxWidth()
            .padding(top = 80.dp)
    ) {
        Icon(
            imageVector = Icons.Default.Info,
            contentDescription = "Empty Illustration",
            tint = Color.Gray,
            modifier = Modifier
                .padding(16.dp)
                .size(48.dp),
        )
        Text(
            text = "No Item Found",
            fontSize = 16.sp,
            color = Color.Gray,
            textAlign = TextAlign.Center,
            modifier = Modifier.padding(horizontal = 16.dp)
        )

        Text(
            text = "Retry",
            fontSize = 16.sp,
            color = colorPrimary,
            textAlign = TextAlign.Center,
            modifier = Modifier
                .padding(horizontal = 16.dp)
                .clickable {
                    onRetry()
                }
        )
    }
}