package com.example.news.feature.news.screens

import android.annotation.SuppressLint
import androidx.activity.ComponentActivity
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.heightIn
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.*
import androidx.compose.material.pullrefresh.PullRefreshIndicator
import androidx.compose.material.pullrefresh.pullRefresh
import androidx.compose.material.pullrefresh.rememberPullRefreshState
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavGraphBuilder
import androidx.navigation.compose.composable
import base.compose.whiteF5F5F5
import com.example.news.base.compose.component.NewsEditText
import com.example.news.base.compose.component.ShowSlideLeft
import com.example.news.base.extensions.asState
import com.example.news.base.extensions.collectResource
import com.example.news.base.extensions.showToast
import com.example.news.feature.news.NewViewModel
import com.example.news.feature.news.component.ColumnList
import com.example.news.feature.news.navigator.SOURCE_LIST_SCREEN

@ExperimentalMaterialApi
@SuppressLint("UnusedMaterialScaffoldPaddingParameter")
fun NavGraphBuilder.SourceListScreen(
    newsVm: NewViewModel,
    onSelectedSource: () -> Unit
) {
    composable(route = SOURCE_LIST_SCREEN) {

        val (_, isError,
            isLoading, data,
            _, error) = newsVm.sourcesData.collectResource().asState()
        val context = LocalContext.current as ComponentActivity

        LaunchedEffect(key1 = true) {
            if(data == null)
                newsVm.getSources()
        }
        LaunchedEffect(key1 = isError) {
            if (isError) context.showToast(error?.message ?: "Fail to fetch source list")
        }
        val refreshState = rememberPullRefreshState(refreshing = isLoading, onRefresh = {
            newsVm.getSources()
        })

        var keySearch by remember {
            mutableStateOf("")
        }

        Column(
            modifier = Modifier
        ) {

            NewsEditText(
                Modifier
                    .padding(8.dp)
                    .border(1.dp, Color.Gray, RoundedCornerShape(8.dp))
                    .clip(RoundedCornerShape(8.dp))
                    .background(whiteF5F5F5.copy(alpha = 0.75f))
                    .padding(8.dp),
                onValueChange = {keySearch = it},
                placeholder = "Search your news provider"
            )

            Box(
                modifier = Modifier
                    .heightIn(min = 120.dp)
                    .fillMaxWidth()
                    .verticalScroll(rememberScrollState())
                    .pullRefresh(refreshState)
            ){

                if (data?.sources.isNullOrEmpty() && !isLoading) {
                    EmptyIllustration {
                        newsVm.getSources();
                    }
                }

                ColumnList(tasks = data?.sources ?: listOf()) { it, index ->
                    if(it.name?.contains(keySearch, true) == false && keySearch != "")
                        return@ColumnList
                    ShowSlideLeft(delay = 200) {
                        Card(
                            Modifier
                                .padding(16.dp)
                                .clickable {
                                    newsVm.reqHeadline.sources = it.id.toString()
                                    onSelectedSource()
                                }
                        ) {
                            Text(
                                text = it.name ?: "Empty",
                                textAlign = TextAlign.Left,
                                fontWeight = FontWeight.Bold,
                                fontSize = 20.sp,
                                modifier = Modifier
                                    .fillMaxWidth()
                                    .padding(vertical = 8.dp, horizontal = 8.dp)
                            )
                        }
                    }
                }

                PullRefreshIndicator(
                    refreshing = isLoading,
                    state = refreshState,
                    modifier = Modifier.align(Alignment.TopCenter),
                )
            }
        }
    }
}