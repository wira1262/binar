package com.example.news.feature.news.screens

import android.annotation.SuppressLint
import android.view.ViewGroup
import android.webkit.WebChromeClient
import android.webkit.WebView
import android.webkit.WebViewClient
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.viewinterop.AndroidView
import androidx.navigation.NavGraphBuilder
import androidx.navigation.compose.composable
import com.example.news.feature.news.NewViewModel
import com.example.news.feature.news.navigator.WEBVIEW_NEWS
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch


@SuppressLint("UnusedMaterialScaffoldPaddingParameter")
fun NavGraphBuilder.WebviewNewsScreen(
    newsVm : NewViewModel,
){
    composable(route = WEBVIEW_NEWS){
        var isLoading by remember {
            mutableStateOf(true)
        }
        val scope = rememberCoroutineScope()
        Box {

            AndroidView(
                factory = { context ->
                    WebView(context).apply {
                        layoutParams = ViewGroup.LayoutParams(
                            ViewGroup.LayoutParams.MATCH_PARENT,
                            ViewGroup.LayoutParams.MATCH_PARENT
                        )
                        webViewClient = WebViewClient()
                        webChromeClient = object : WebChromeClient() {
                            override fun onProgressChanged(view: WebView, progress: Int) {
                                if(progress >= 100){
                                    scope.launch {
                                        delay(1000)
                                        isLoading = false
                                    }
                                }
                            }
                        }
                        val url = (newsVm.currentSelectedNew.url ?: "").let {
                            if(it.contains("https")) return@let it
                            return@let it.replace("http", "https")
                        }
                        loadUrl(url)
                    }
                },
                modifier = Modifier.fillMaxSize()
            )

            if(isLoading)
                CircularProgressIndicator(Modifier.align(Alignment.Center))
        }
    }
}