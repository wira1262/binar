@file:OptIn(ExperimentalMaterialApi::class, ExperimentalMaterialApi::class)

package com.example.news.feature.news.component

import androidx.activity.compose.BackHandler
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.itemsIndexed
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.ExperimentalMaterialApi
import androidx.compose.material.Text
import androidx.compose.material.pullrefresh.PullRefreshIndicator
import androidx.compose.material.pullrefresh.pullRefresh
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import base.compose.white
import base.compose.whiteF5F5F5
import com.example.news.base.compose.component.BackgroundGradationBox
import com.example.news.base.compose.component.ImageUrlLoader
import com.example.news.base.compose.component.NewsEditText
import com.example.news.base.compose.component.ShowFadeInSlideBottom
import com.example.news.base.compose.component.lazyColumnControl
import com.example.news.base.extensions.asState
import com.example.news.base.extensions.collectEventState
import com.example.news.base.extensions.showToast
import com.example.news.data.remote.model.response.Articles
import com.example.news.feature.news.NewViewModel
import com.example.news.feature.news.screens.EmptyIllustration
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch


@Composable
fun <Data> ColumnList(
    tasks: List<Data>,
    onDrawItem: @Composable (Data, Int) -> Unit,
) {
    Column() {
        tasks.forEachIndexed { index, it ->
            onDrawItem(it, index)
        }
    }
}

@ExperimentalMaterialApi
@Composable
fun NewsList(
    tasks: List<Articles>,
    viewModel: NewViewModel,
    onSelectTask: (Articles) -> Unit,
    onBack : ()->Unit,
) {
    val context = LocalContext.current
    val coroutineScope = rememberCoroutineScope()
    fun doFetch(target: Int) {
        viewModel.reqHeadline.apply { page = target }
        viewModel.getNews()
    }
    val (appendData, releasePagingBlock, scrollState, listItem,
        reset, refreshState, isLoading, setLoading) =
        lazyColumnControl(
            tasks, ::doFetch, ::doFetch
        )

    val (_, isError, _, data, _, error) =
        viewModel.newsData.collectEventState().asState()

    LaunchedEffect(key1 = data) {
        coroutineScope.launch {
            delay(250)
            setLoading(false)
        }
        if(data == null) return@LaunchedEffect
        if (data.articles.isEmpty()) {
            if (isError) releasePagingBlock();
            context.showToast("No More news available")
            return@LaunchedEffect
        }
        appendData(data?.articles ?: listOf())
        releasePagingBlock()
    }

    LaunchedEffect(key1 = isError) {
        if (isError) {
            coroutineScope.launch {
                delay(250)
                setLoading(false)
            }
            context.showToast(error?.message ?: "There's network error")
        }
    }

    LaunchedEffect(key1 = true) {
        if (data == null) {
            setLoading(true)
            doFetch(1)
        }
    }

    BackHandler(true, onBack = {
        viewModel.reqHeadline.keyword = ""
        viewModel.reqHeadline.page = 0
        onBack()
    })


    Box(
        Modifier
            .pullRefresh(refreshState)
            .fillMaxSize()
    ) {

        LazyColumn(
            Modifier.pullRefresh(
                refreshState
            ), scrollState
        ) {
            item {
                if (isLoading) return@item
                if (listItem.isNotEmpty()) return@item
                EmptyIllustration {
                    setLoading(true)
                    viewModel.getNews()
                }
            }
            itemsIndexed(items = listItem, itemContent = {index , it->
                Box(
                    Modifier.fillMaxSize().height(200.dp)
                ){
                    ShowFadeInSlideBottom(delay = 300) {
                        ContentNews(articles = it, {
                            onSelectTask(it)
                        }, true)
                    }
//                    if(index % 2 == 0){
//                        ShowSlideLeft(delay = 100) {
//                            ContentNews(articles = it, {
//                                onSelectTask(it)
//                            }, true)
//                        }
//                    } else {
//                        ShowSlideLeft(delay = 100, isFromRight = true) {
//                            ContentNews(articles = it, {
//                                onSelectTask(it)
//                            }, false)
//                        }
//                    }

                }
            })
        }

        PullRefreshIndicator(
            refreshing = isLoading,
            state = refreshState,
            modifier = Modifier
                .align(Alignment.TopCenter)
                .padding(60.dp),
        )

        NewsEditText(
            Modifier
                .padding(8.dp)
                .border(1.dp, Color.Gray, RoundedCornerShape(8.dp))
                .clip(RoundedCornerShape(8.dp))
                .background(whiteF5F5F5.copy(alpha = 0.75f))
                .padding(8.dp)
                .align(Alignment.TopCenter),
            onValueChange = {
                setLoading(true)
                viewModel.reqHeadline.keyword = it
                viewModel.reqHeadline.page = 0
                reset()
                viewModel.getNews()
            },
            placeholder = "Search your news here"
        )
    }

}


@Composable
fun ContentNews(
    articles: Articles, onSelectTask: (Articles) -> Unit,
    isRight: Boolean
) {
    Box(
        Modifier.clickable { onSelectTask(articles) },
        contentAlignment = if (!isRight) Alignment.CenterStart
        else Alignment.CenterEnd
    ) {
        ImageUrlLoader(
            articles.urlToImage ?: "",
            articles.title ?: ""
        )
        BackgroundGradationBox(
            Modifier
                .height(200.dp)
                .fillMaxWidth(),
            isRight
        ) {
            Text(
                text = articles.title ?: "",
                textAlign = TextAlign.Justify,
                fontWeight = FontWeight.Bold,
                fontSize = 20.sp,
                color = white,
                overflow = TextOverflow.Ellipsis,
                modifier = Modifier
                    .padding(
                        top = 24.dp, bottom = 24.dp,
                        start = if (isRight) 0.dp else 8.dp,
                        end = if (isRight) 8.dp else 0.dp,
                    )
                    .fillMaxWidth(0.5f)
            )
        }
    }
}
