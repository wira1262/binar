package com.example.news.feature.news

import androidx.lifecycle.ViewModel
import com.example.news.data.remote.model.request.GetHeadlinesRequest
import com.example.news.data.remote.model.response.Articles
import com.example.news.domain.usecase.GetNewsUseCase
import com.example.news.domain.usecase.GetSourceUseCase

class NewViewModel(
    val newsUseCase: GetNewsUseCase,
    val getSourceUsecase: GetSourceUseCase
): ViewModel() {

    val newsData = newsUseCase.currentData

    var reqHeadline = GetHeadlinesRequest()
    fun getNews() = newsUseCase.setup(reqHeadline)

    val sourcesData = getSourceUsecase.currentData
    fun getSources() = getSourceUsecase.setup(reqHeadline.category)

    var currentSelectedNew : Articles = Articles()
}