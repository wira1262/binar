package com.example.news.feature.news.navigator

import androidx.compose.material.ExperimentalMaterialApi
import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import com.example.news.feature.news.NewViewModel
import com.example.news.feature.news.screens.CategoryListScreen
import com.example.news.feature.news.screens.NewsListScreen
import com.example.news.feature.news.screens.SourceListScreen
import com.example.news.feature.news.screens.WebviewNewsScreen

@ExperimentalMaterialApi
@Composable
fun NewsComposeNavigationHost(
    newsViewModel: NewViewModel,
    nav: NavHostController,
){
    val navigator = remember(nav){
        TaskScreenNavigator(nav)
    }

    NavHost(nav, startDestination = CATEGORY_LIST_SCREEN){
        CategoryListScreen(newsViewModel) {
            navigator.navigateToSourceList()
        }
        SourceListScreen(newsViewModel) {
            navigator.navigateToMain()
        }
        NewsListScreen(newsViewModel, {
            navigator.navigateToWebviewNews()
        }) {
            navigator.pop()
        }
        WebviewNewsScreen(newsViewModel)
    }
}

val CATEGORY_LIST_SCREEN by lazy { "CATEGORY_LIST_SCREEN" }
val SOURCE_LIST_SCREEN by lazy { "SOURCE_LIST_SCREEN" }
val NEWS_LIST_SCREEN by lazy { "NEWS_LIST_SCREEN" }
val NEWS_CONTENT_SCREEN by lazy {"NEWS_CONTENT_SCREEN"}
val WEBVIEW_NEWS by lazy {"WEBVIEW_NEWS"}