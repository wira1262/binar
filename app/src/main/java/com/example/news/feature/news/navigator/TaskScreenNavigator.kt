package com.example.news.feature.news.navigator

import androidx.navigation.NavHostController

class TaskScreenNavigator(
    val nav : NavHostController
){
    fun navigateToSourceList() {
        nav.navigate(
            route = SOURCE_LIST_SCREEN
        )
    }
    fun navigateToMain (isPop : Boolean = false) {
        nav.navigate(
            route = NEWS_LIST_SCREEN
        ){ if(isPop) popUpTo(NEWS_LIST_SCREEN) }
    }

    fun pop(){
        nav.popBackStack()
    }
    fun navigateToWebviewNews(){
        nav.navigate(WEBVIEW_NEWS)
    }
}