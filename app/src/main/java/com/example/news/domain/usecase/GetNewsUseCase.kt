package com.example.news.domain.usecase

import com.example.news.base.base_classes.BaseUseCase
import com.example.news.data.remote.api.NewsApi
import com.example.news.data.remote.model.request.GetHeadlinesRequest
import com.example.news.data.remote.model.response.NewsResponse
import com.example.news.data.remote.model.response.SourceResponseDto


class GetNewsUseCase(
    val teaApi: NewsApi
) : BaseUseCase<GetHeadlinesRequest, NewsResponse>() {
    override fun setup(parameter: GetHeadlinesRequest) {
        super.setup(parameter)
        execute {
            return@execute teaApi.getHeadlines(parameter)
        }
    }
}

class GetSourceUseCase(
    val teaApi: NewsApi
) : BaseUseCase<String, SourceResponseDto>() {
    override fun setup(parameter: String) {
        super.setup(parameter)
        execute {
            return@execute teaApi.getSources(parameter)
        }
    }
}

